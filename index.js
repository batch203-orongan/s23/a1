/*
3. Create a trainer object using object literals.
4. Initialize/add the following trainer object properties:
a. Name (String)
b. Age (Number)
c. Pokemon (Array)
d. Friends (Object with Array values for properties)
5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
6. Access the trainer object properties using dot and square bracket notation.
7. Invoke/call the trainer talk object method.
*/

let trainer = {
		name: "Ash Ketchum",
		age: 10,
		pokemon : ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
		friends: {
			hoen: ["May", "Max"],
			kanto: ["Brock", "Misty"],
		},
		talk: function(){
			console.log(this.pokemon[0]+"! I choose you!");
		}
};
console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);
console.log("Result of talk method:");
trainer.talk();
